FROM cloudron/base:3.2.0@sha256:ba1d566164a67c266782545ea9809dc611c4152e27686fd14060332dd88263ea

RUN mkdir -p /app/data /app/code /app/data/staticfiles /app/data/micropub_media 

WORKDIR /app/data

RUN virtualenv -p python3 /app/code/env
ENV VIRTUAL_ENV=/app/code/env
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

COPY . /app/data
COPY requirements.txt .
COPY requirements_dev.txt .
COPY requirements.lock .

RUN chown -R cloudron:cloudron /app/code
RUN chown -R cloudron:cloudron /app/data

RUN \
  apt-get update && \
  apt-get install -y spatialite-bin libsqlite3-mod-spatialite \
     binutils libproj-dev gdal-bin && \
  pip install -U pip && pip install pipenv && \
  pip install -r requirements_dev.txt && \
  rm -rf /var/lib/apt/lists/*

WORKDIR /app/data

RUN chown -R cloudron:cloudron /app/data
RUN chmod +x start.sh

EXPOSE 8000

CMD [ "./start.sh" ]
