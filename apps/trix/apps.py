from django.apps import AppConfig


class TrixConfig(AppConfig):
    name = "trix"
