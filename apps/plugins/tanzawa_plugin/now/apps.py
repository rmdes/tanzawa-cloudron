from django import apps


class NowConfig(apps.AppConfig):
    name = "plugins.tanzawa_plugin.now"
