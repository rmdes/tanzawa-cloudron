#!/bin/bash

set -eu

exec python3 -c "import secrets; print(secrets.token_urlsafe())" | xargs -I{} -n1 echo SECRET_KEY={} >> .env

source .env

echo "==> Preparing the DB"
exec python3 apps/manage.py migrate

# apply migrations
exec python3 apps/manage.py migrate

#Do the superuser manually via the web terminal
#python3 apps/manage.py createsuperuser

echo "==> Starting Tanzawa"
exec python3 apps/manage.py runserver 0.0.0.0:8000
